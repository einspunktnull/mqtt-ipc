export class DoubleMap<K1, K2, V> {

    private readonly map: Map<K1, Map<K2, V>> = new Map<K1, Map<K2, V>>();

    public set(k1: K1, k2: K2, v: V): this {
        this.getK1(k1).set(k2, v);
        return this;
    }

    public get(k1: K1, k2: K2): V | undefined {
        return this.getK1(k1).get(k2);
    }

    public delete(k1: K1, k2: K2): boolean {
        const deleted: boolean = this.getK1(k1).delete(k2);
        if (this.getK1(k1).size === 0) this.map.delete(k1);
        return deleted;
    }

    public has(k1: K1, k2: K2): boolean {
        return this.getK1(k1).has(k2);
    }

    public forEach(callbackfn: (v: V, k1: K1, k2: K2) => void, thisArg?: any): void {
        this.map.forEach((m: Map<K2, V>, k1: K1) => {
            m.forEach((v: V, k2: K2) => {
                callbackfn.apply(thisArg, [v, k1, k2]);
            }, thisArg);
        });
    }

    public get values(): Array<V> {
        const arr: Array<V> = [];
        this.forEach((v: V) => {
            arr.push(v);
        });
        return arr;
    }

    private getK1(k1: K1): Map<K2, V> {
        if (this.map.get(k1) === undefined) this.map.set(k1, new Map<K2, V>());
        return this.map.get(k1);
    }

}
