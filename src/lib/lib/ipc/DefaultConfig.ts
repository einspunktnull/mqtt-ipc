import {Config} from './types';

export const DefaultConfig: Config = {
    retry: 1500,
    maxRetries: 1,
    silent: true
};
