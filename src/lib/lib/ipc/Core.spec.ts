import 'jest-extended';
import {Core} from './Core';
import {Callback} from 'beautiful-types';

describe('Core', () => {

    it('Core', async done => {
        const server: Core = new Core('server', 'server');
        expect(() => server.send('bla', '123', 'blubb')).toThrow(Core.ERROR_NOT_INITIALIZED);
        expect(() => server.on('bla', console.log)).toThrow(Core.ERROR_NOT_INITIALIZED);
        expect(() => server.once('bla', console.log)).toThrow(Core.ERROR_NOT_INITIALIZED);
        expect(() => server.off('bla')).toThrow(Core.ERROR_NOT_INITIALIZED);
        await expect(server.initialize()).resolves.toBeUndefined();
        await expect(server.initialize()).rejects.toThrow(Core.ERROR_ALREADY_INITIALIZED);

        const client: Core = new Core('server', 'client');
        await expect(client.initialize()).resolves.toBeUndefined();
        await expect(client.initialize()).rejects.toThrow(Core.ERROR_ALREADY_INITIALIZED);

        const serverCb: Callback = (data) => {
            try {
                expect(data.data).toEqual('geil');
                const clientCb: Callback = (data) => {
                    try {
                        expect(data.data).toEqual('nuok');
                        done();
                    } catch (error) {
                        done(error);
                    }
                };
                client.on('blo', clientCb);
                server.send('blo', '123', 'nuok');
            } catch (error) {
                done(error);
            }
        };
        server.on('bla', serverCb);
        client.send('bla', '123', 'geil');

    });

});
