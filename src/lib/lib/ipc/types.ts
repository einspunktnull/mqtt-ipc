import nodeIpc from 'node-ipc';
import {Callback, OrPromise} from 'beautiful-types';
import {Socket} from 'net';

export type Config = Partial<typeof nodeIpc.config>;
export type Server = Partial<typeof nodeIpc.server>;
export type Client = {
    on(event: string, callback: Callback): void;
    emit(event: string, value: any): Client;
    off(event: string, handler: any): Client;
    off(event: string, handler: any): Client;
};
export type ClientOrServer = Client | Server;
export type Dto<T> = { msgId: string; sender: string; data?: T; };
export type EventCallback<T> = (data: Dto<T>, socket?: Socket) => void;
export type Subscription<T> = { id: string; event: string; callback: EventCallback<T> };
export type CreateAnswerValueFct<Tin, Tout> = (sender: string, data: Tin) => OrPromise<Tout>;
export type Subscriber<T> = (data: T) => void;
