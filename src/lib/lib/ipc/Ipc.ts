import {OrPromise, Reject, Resolve} from 'beautiful-types';
import {Core} from './Core';
import {CreateAnswerValueFct, Dto, Subscriber} from './types';
import {v1 as uuidV1} from 'uuid';

export class Ipc {

    private core: Core;

    public constructor(serverId: string, id: string) {
        this.core = new Core(serverId, id);
    }

    public get id(): string {
        return this.core.id;
    }

    public get serverId(): string {
        return this.core.serverId;
    }

    public get isServer(): boolean {
        return this.core.isServer;
    }

    public async initialize(): Promise<this> {
        await this.core.initialize();
        return this;
    }

    public answer<Tin, Tout>(event: string, answerValueFct: CreateAnswerValueFct<Tin, Tout>): void {
        // console.log('IpcService.answer()', this.id, topic, data);
        this.core.on(
            Helper.asQuestionEvent(event),
            async (dto: Dto<Tin>) => {
                const answerFctReturnValue: OrPromise<Tout> = answerValueFct(
                    dto.sender,
                    dto.data,
                );
                const answerData: Tout = answerFctReturnValue instanceof Promise ?
                    await answerFctReturnValue :
                    answerFctReturnValue;
                this.core.send(
                    Helper.asAwnserEvent(event),
                    dto.msgId,
                    answerData
                );
            }
        );
    }

    public ask<Tout, Tin>(event: string, data?: Tout, timeoutMs: number = 100): Promise<Tin> {
        // console.log('IpcService.ask()', this.id, topic, data);
        return new Promise<Tin>((resolve: Resolve<Tin>, reject: Reject) => {
            let timeout: NodeJS.Timeout;
            const msgId: string = uuidV1();
            const subId: string = this.core.on(
                Helper.asAwnserEvent(event),
                (dto: Dto<Tin>) => {
                    if (dto.msgId === msgId) {
                        resolve(dto.data);
                        this.core.off(subId);
                        clearTimeout(timeout);
                    }
                }
            );
            this.core.send(Helper.asQuestionEvent(event), msgId, data);
            timeout = setTimeout(() => {
                this.core.off(subId);
                reject(new Error('ask timed out'));
            }, timeoutMs);
        });
    }

    public sendTo<Tout>(receiverId: string, event: string, data?: Tout): void {
        // console.log('Ipc.sendTo', receiverId, event, data);
        this.core.sendTo(receiverId, event, data);
    }

    public subscribe<Tin>(event: string, subscriber: Subscriber<Tin>): string {
        return this.core.on(event, (dto: Dto<Tin>) => {
            // console.log('Ipc.on', event, dto);
            subscriber(dto.data);
        });
    }

}

class Helper {

    public static asQuestionEvent(topic: string): string {
        return `QUESTION_${topic}`;
    }

    public static asAwnserEvent(topic: string): string {
        return `ANSWER_${topic}`;
    }
}
