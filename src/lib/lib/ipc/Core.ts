import nodeIpc from 'node-ipc';
import {Reject, Resolve} from 'beautiful-types';
import {Socket} from 'net';
import {v1 as uuidV1} from 'uuid';
import assert from 'assert';
import {Client, ClientOrServer, Dto, EventCallback, Server, Subscription} from './types';
import {DefaultConfig} from './DefaultConfig';

export class Core {

    public static readonly ERROR_NOT_INITIALIZED: string = 'ERROR_NOT_INITIALIZED';
    public static readonly ERROR_ALREADY_INITIALIZED: string = 'ERROR_ALREADY_INITIALIZED';
    public static readonly ERROR_ALREADY_INITIALIZING: string = 'ERROR_ALREADY_INITIALIZING';

    private readonly _serverId: string;
    private startResolve: Resolve<void>;
    private startReject: Reject;
    private _ipcInstance: ClientOrServer;

    private _initialized: boolean = false;
    private _initializing: boolean = false;

    private readonly _id: string;
    private readonly subs: Map<string, Subscription<any>> = new Map<string, Subscription<any>>();
    private readonly socketsMap: Map<string, Socket> = new Map<string, Socket>();
    private readonly sockets: Socket[] = [];

    public constructor(serverId: string, id: string) {
        this._serverId = serverId;
        this._id = id;
        nodeIpc.config.id = this.id;
        for (const key in DefaultConfig) {
            nodeIpc.config[key] = DefaultConfig[key];
        }
    }

    public async initialize(): Promise<void> {
        // console.log('this._initialized', this._initialized);
        assert(!this._initialized, Core.ERROR_ALREADY_INITIALIZED);
        assert(!this._initializing, Core.ERROR_ALREADY_INITIALIZING);
        this._initializing = true;
        return new Promise<void>(async (resolve: Resolve<void>, reject: Reject) => {
            this.startResolve = resolve;
            this.startReject = reject;
            this.isServer ? this.startServer() : this.startClient();
        });
    }

    public get id(): string {
        return this._id;
    }

    public get serverId(): string {
        return this._serverId;
    }

    public get isServer(): boolean {
        return this.serverId === this.id;
    }

    public send<T>(event: string, msgId: string, data: T): void {
        this.assertInitialized();
        // console.log('Core.send()', this.id, topic, data);
        const dto: Dto<T> = {
            msgId: msgId,
            sender: this.id,
            data: data,
        };
        if (this.isServer) {
            this.sockets.forEach((sock: Socket) => {
                this.server.emit(sock, event, dto);
            });
        } else {
            this.client.emit(event, dto);
        }
    }

    public sendTo<T>(receiverId: string, event: string, data: T): void {
        this.assertInitialized();
        const dto: Dto<T> = {
            msgId: uuidV1(),
            sender: this.id,
            data: data,
        };
        if (this.isServer) {
            // console.log('isServer', this.id, event, dto);
            const sock: Socket = this.socketsMap.get(receiverId);
            if (sock) {
                // console.log('isSock', sock, event, dto);
                this.server.emit(sock, event, dto);
            }
        } else throw new Error('only Server can send to specific target');
    }

    public once<T>(event: string, onCallback: EventCallback<T>): void {
        this.assertInitialized();
        // console.log('IpcCore.once()', this.id, topic);
        const subId: string = this.on(event, (data: Dto<T>) => {
            onCallback(data);
            this.off(subId);
        });
    }

    public on<T>(event: string, onCallback: EventCallback<T>): string {
        this.assertInitialized();
        // console.log('IpcCore.on()', this.id, event);
        const id: string = uuidV1();
        const onEvent: EventCallback<T> = (dto: Dto<T>, socket?: Socket) => {
            if (!!socket && this.socketsMap.get(dto.sender) === undefined)
                this.socketsMap.set(dto.sender, socket);
            // console.log('IpcCore.onEvent()', this.id,nodeIpc);
            onCallback(dto, socket);
        };
        this.subs.set(id, {
            id: id,
            callback: onEvent,
            event: event
        });
        this._ipcInstance.on(event, onEvent);
        return id;
    }

    public off<T>(subId: string): void {
        this.assertInitialized();
        // console.log('IpcCore.off()', this.id, subId);
        const sub: Subscription<T> = this.subs.get(subId);
        if (sub) {
            this.subs.delete(sub.id);
            this._ipcInstance.off(sub.event, sub.callback);
        }
    }

    private get clientOrServer(): ClientOrServer {
        return this._ipcInstance;
    }

    private get client(): Client | undefined {
        return this.isServer ? undefined : <Client>this.clientOrServer;
    }

    private get server(): Server | undefined {
        return this.isServer ? <Server>this.clientOrServer : undefined;
    }

    private set clientOrServer(clientOrServer: ClientOrServer) {
        this._ipcInstance = clientOrServer;
    }

    private set server(server: Server) {
        this.clientOrServer = server;
    }

    private set client(client: Client) {
        this.clientOrServer = client;
    }

    private startClient(): void {
        const serverId: string = this._serverId;
        nodeIpc.connectTo(serverId, () => {
            this.client = nodeIpc.of[this._serverId];
            this.registerEventListeners();
        });
    }

    private startServer(): void {
        // console.log('startServer',this.id);
        nodeIpc.serve(() => {
            if (this.startResolve) {
                this.startResolve();
                this.startResolve = undefined;
                this._initializing = false;
                this._initialized = true;
            }
        });
        this.server = nodeIpc.server;
        this.registerEventListeners();
        this.server.start();
    }

    private registerEventListeners(): void {
        this.clientOrServer.on("error", this.onError.bind(this));
        this.clientOrServer.on("connect", this.onConnect.bind(this));
        this.clientOrServer.on("disconnect", this.onDisconnect.bind(this));
        this.clientOrServer.on("data", this.onData.bind(this));
    }

    private unregisterEventListeners(): void {
        this.clientOrServer.off("error", this.onError.bind(this));
        this.clientOrServer.off("connect", this.onConnect.bind(this));
        this.clientOrServer.off("disconnect", this.onDisconnect.bind(this));
        this.clientOrServer.off("data", this.onData.bind(this));
        this.subs.forEach((sub: Subscription<any>) => {
            this.clientOrServer.off(sub.event, sub.callback);
        });
    }

    private onConnect(socket: Socket): void {
        // console.log('this.onConnect', this.isServer, this.id,ddd);
        if (socket) this.sockets.push(socket);
        const resolve: Resolve<void> = this.startResolve;
        if (!this.isServer) {
            this._initializing = false;
            this._initialized = true;
        }
        this.startResolve = undefined;
        if (resolve) resolve();

    }

    private onError(err: Error): void {
        // console.log('IpcCore.onError', this.id, err);
        const reject: Reject = this.startReject;
        if (!this.startReject) throw err;
        this._initializing = false;
        this._initialized = false;
        this.startReject = undefined;
        this.die();
        reject(err);
    }

    private onDisconnect(): void {
        if (!this._initialized) return;
        // console.log('IpcCore.onDisconnect', this._initialized);
    }

    private onData(data: any): void {
        console.log('IpcCore.onData()', this.id, data);
    }

    private die() {
        try {
            this._initializing = false;
            this._initialized = false;
            this.unregisterEventListeners();
            this.server?.stop();
            nodeIpc.disconnect(this._serverId);
            nodeIpc.disconnect(this._serverId);
        } catch (e) {
            console.error(e);
        }
    }

    private assertInitialized(): void {
        assert(this._initialized === true, Core.ERROR_NOT_INITIALIZED);
    }

}

