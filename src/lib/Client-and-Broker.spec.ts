import {Client} from './Client';
import {Broker} from './Broker';

describe('Client', () => {

    it('Client.constructor()', async () => {
        expect(() => { new Client('broker', 'broker'); }).toThrow(Client.ERROR_SAME_ID);
        expect(() => { new Client('broker', 'client'); }).not.toThrow();
    });

    it('Broker/Client.initialize()', async () => {

        // const broker: Broker = new Broker('broker');
        // await expect(broker.initialize()).resolves.toBeUndefined();
        //
        // const client1: Client = new Client('broker', 'client1');
        // await expect(client1.initialize()).resolves.toBeUndefined();
        //
        // const client2: Client = new Client('broker', 'client2');
        // await expect(client2.initialize()).resolves.toBeUndefined();
        //
        // const client3: Client = new Client('broker', 'client3');
        // await expect(client3.initialize()).resolves.toBeUndefined();

        // client1.subscribe('uwe/+/text',(d)=>{
        //     console.log('uwe/+/text',d);
        // });
        // client2.publish('uwe/ja/text');
        // client3.publish('uwe/nein/text');
    });

});
