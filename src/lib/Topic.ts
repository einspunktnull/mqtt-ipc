import assert from 'assert';

export class Topic {

    // public static readonly REGEX_WORD: RegExp = /[\d\w]+/;
    // public static readonly REGEX_PLUS: RegExp = /\+/;
    // public static readonly REGEX_SLASH: RegExp = /\//;
    // public static readonly REGEX_SLASH_WITHOUT_FOLLOWING_HASH: RegExp = /\/(?!#)/;
    // public static readonly REGEX_SLASH_WITH_FOLLOWING_HASH: RegExp = /\/#/;
    // public static readonly REGEX_HASH: RegExp = /#/;

    public static readonly ERROR_INVALID_TOPIC: string = 'invalid topic';
    public static readonly ERROR_INVALID_TOPIC_PUBLISH: string = 'invalid publish topic';
    public static readonly ERROR_INVALID_TOPIC_SUBSCRIBE: string = 'invalid subscribe topic';

    public static readonly REGEX_TOPIC_SUBSCRIBE: RegExp = /^(?!#)(?:(?:\+)|(?:(?:(?:[\w\d]+|\+)\/)*(?:[\w\d]+|#|\+)))$/;
    public static readonly REGEX_TOPIC_PUBLISH: RegExp = /^(?:[\d\w]+\/)*(?:[\d\w]+\/?)$/;

    public static readonly REGEX_BEGIN: RegExp = /^/;
    public static readonly REGEX_END: RegExp = /$/;
    public static readonly REGEX_PLUSES: RegExp = /\+/g;
    public static readonly REGEX_WORD: RegExp = /[\d\w]+/;
    public static readonly REGEX_ANY: RegExp = /[\d\w\/]*/;
    public static readonly REGEX_ENDS_WITH_SLASH_AND_HASH: RegExp = /\/#$/;

    public static validatePublish(topic: string): boolean {
        return this.REGEX_TOPIC_PUBLISH.test(topic);
    }

    public static validateSubscribe(topic: string): boolean {
        return this.REGEX_TOPIC_SUBSCRIBE.test(topic);
    }

    public static validate(topic: string): boolean {
        return this.validateSubscribe(topic) || this.validatePublish(topic);
    }

    public static match(topicSubscribe: string, topicPublish: string) {
        assert(this.validateSubscribe(topicSubscribe), this.ERROR_INVALID_TOPIC_SUBSCRIBE);
        assert(this.validatePublish(topicPublish), this.ERROR_INVALID_TOPIC_PUBLISH);
        return this.toRegex(topicSubscribe).test(topicPublish);
    }

    public static toRegex(topic: string): RegExp {
        assert(this.validate(topic), this.ERROR_INVALID_TOPIC);
        const regexStr = topic
            .replace(this.REGEX_BEGIN, this.REGEX_BEGIN.source)
            .replace(this.REGEX_PLUSES, this.REGEX_WORD.source)
            .replace(this.REGEX_ENDS_WITH_SLASH_AND_HASH, this.REGEX_ANY.source)
            .replace(this.REGEX_END, this.REGEX_END.source);
        return new RegExp(regexStr);
    }

}

