import {MqttIpc} from './MqttIpc';
import {v1 as uuidV1} from 'uuid';
import {
    EVENT_PUBLISH,
    EVENT_SUBSCRIBE,
    EVENT_UNSUBSCRIBE,
    PubAskDto,
    PubAnswerDto,
    SubAskDto,
    SubAnswerDto,
    UnsubAskDto,
    UnsubAnswerDto,
} from './common';
import {Topic} from './Topic';
import {DoubleMap} from './lib/DoubleMap';

type Sub = {
    uuid: string;
    topic: string;
    subscriberId: string;
};

export class Broker extends MqttIpc {

    private readonly subs: DoubleMap<string, string, Sub> = new DoubleMap<string, string, Sub>();

    public constructor(brokerId: string) {
        super(brokerId, brokerId);
    }

    public async initialize(): Promise<void> {
        await super.initialize();
        this.ipc.answer(EVENT_SUBSCRIBE, this.subscribe.bind(this));
        this.ipc.answer(EVENT_UNSUBSCRIBE, this.unsubscribe.bind(this));
        this.ipc.answer(EVENT_PUBLISH, this.publish.bind(this));
    }

    private subscribe(subscriberId: string, subAskDto: SubAskDto): SubAnswerDto {
        // console.log('Broker.subscribe', subscriberId, subAskDto);
        const topic: string = subAskDto.topic;
        if (!Topic.validateSubscribe(topic)) return {
            success: false,
            error: `${Topic.ERROR_INVALID_TOPIC_SUBSCRIBE}: ${topic}`,
            topic: topic,
            uuid: null
        };
        const uuid: string = uuidV1();
        const sub: Sub = {
            uuid: uuid,
            subscriberId: subscriberId,
            topic: topic,
        };
        this.subs.set(topic, subscriberId, sub);
        return {
            success: true,
            error: null,
            topic: topic,
            uuid: uuid,
        };
    }

    private unsubscribe(unsubscriberId: string, unsubAskDto: UnsubAskDto): UnsubAnswerDto {
        // console.log('Broker.unsubscribe', unsubscriberId, unsubAskDto);
        const topic: string = unsubAskDto.topic;
        const sub: Sub = this.subs.get(topic, unsubscriberId);
        this.subs.delete(topic, unsubscriberId);
        return {
            success: true,
            error: null,
            topic: topic,
            uuid: sub?.uuid ?? null
        };
    }

    private publish<T>(publisherId: string, pubAskDto: PubAskDto<T>): PubAnswerDto<T> {
        // console.log('Broker.publish', publisherId, pubAskDto);
        const topic: string = pubAskDto.topic;
        const data: T = pubAskDto.data;
        if (!Topic.validatePublish(topic)) return {
            success: false,
            error: `${Topic.ERROR_INVALID_TOPIC_PUBLISH}: ${topic}`,
            topic: topic,
            data: null
        };
        this.subs.values
            .filter(sub => Topic.match(sub.topic, topic))
            .forEach(sub => this.ipc.sendTo(
                sub.subscriberId,
                EVENT_PUBLISH,
                data
            ));
        return {
            success: true,
            error: null,
            topic: topic,
            data: data
        };
    }

}
