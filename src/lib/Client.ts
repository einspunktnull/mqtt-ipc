import assert from 'assert';
import {MqttIpc} from './MqttIpc';
import {Subscriber} from './lib/ipc/types';
import {Topic} from './Topic';
import {
    EVENT_PUBLISH,
    EVENT_SUBSCRIBE, EVENT_UNSUBSCRIBE,
    PubAnswerDto,
    PubAskDto,
    SubAnswerDto,
    SubAskDto,
    UnsubAnswerDto,
    UnsubAskDto
} from './common';


export class Client extends MqttIpc {

    private readonly subs: Map<string, Subscriber<unknown>> = new Map<string, Subscriber<unknown>>();

    public static readonly ERROR_SAME_ID: string = 'id must not be same as serverid';

    public constructor(brokerId: string, id: string) {
        super(brokerId, id);
        assert(brokerId !== id, Client.ERROR_SAME_ID);
    }

    public async subscribe<T>(topic: string, callback: Subscriber<T>): Promise<string> {
        assert(Topic.validateSubscribe(topic));
        const answerDto: SubAnswerDto = await this.ipc.ask<SubAskDto, SubAnswerDto>(
            EVENT_SUBSCRIBE, {topic: topic}
        );
        if (!answerDto.success) throw new Error(answerDto.error);
        this.subs.set(topic,callback);
        this.ipc.subscribe(EVENT_PUBLISH, callback);
        return answerDto.uuid;
    }

    public async unsubscribe<T>(topic: string, callbackOrSubId?: Subscriber<T>): Promise<string> {
        assert(Topic.validateSubscribe(topic));
        const answerDto: UnsubAnswerDto = await this.ipc.ask<UnsubAskDto, UnsubAnswerDto>(
            EVENT_UNSUBSCRIBE, {topic: topic}
        );
        if (!answerDto.success) throw new Error(answerDto.error);
        this.subs.delete(topic);
        return answerDto.uuid;
    }

    public async publish<T>(topic: string, data: T): Promise<T> {
        // assert(Topic.validatePublish(topic));
        const answerDto: PubAnswerDto<T> = await this.ipc.ask<PubAskDto<T>, PubAnswerDto<T>>(
            EVENT_PUBLISH,
            {topic: topic, data: data}
        );
        if (!answerDto.success) throw new Error(answerDto.error);
        return answerDto.data;
    }

}
