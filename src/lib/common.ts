export const EVENT_ERROR: string = 'ERROR';
export const EVENT_PUBLISH: string = 'PUBLISH';
export const EVENT_SUBSCRIBE: string = 'SUBSCRIBE';
export const EVENT_UNSUBSCRIBE: string = 'UNSUBSCRIBE';

export interface BaseDto {
    topic: string;
}

export interface SubAskDto extends BaseDto {
}

export interface UnsubAskDto extends BaseDto {
}

export interface PubAskDto<T> extends BaseDto {
    data: T;
}

export interface BaseAnswerDto {
    success: boolean;
    error: string | null;
}

export interface SubAnswerDto extends BaseDto, BaseAnswerDto {
    uuid: string | null;
}

export interface UnsubAnswerDto extends BaseDto, BaseAnswerDto {
    uuid: string | null;
}

export interface PubAnswerDto<T> extends BaseDto, BaseAnswerDto {
    data: T | null;
}
