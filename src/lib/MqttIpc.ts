import {Ipc} from './lib/ipc/Ipc';

export class MqttIpc {

    protected readonly ipc: Ipc;

    protected constructor(brokerId: string, id: string) {
        this.ipc = new Ipc(brokerId, id);
    }

    public async initialize(): Promise<void> {
        await this.ipc.initialize();
    }

}
