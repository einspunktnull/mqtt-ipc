import {Topic} from './Topic';

export const ValidTopicSubscribe: string[] = [
    '+/wohnung1/+/#',
    '+/+/uwe/+/wohnzimmer/#',
    'objekt1/wohnung1/wohnzimmer/feuchte',
    '+',
    '+/#',
    'affe/+/#',
    'affe/+',
    'objekt1/wohnung2/schlafzimmer/#',
    '+/+/+/+/#',
    'objekt1/wohnung2/wohnzimmer/temperatur',
];

export const InvalidTopicSubscribe: string[] = [
    '/#',
    'objekt1/wohnung1/#/feuchte',
    '   ',
    '/dsa',
    '/',
    '+/++/wohnzimmer/#',
    '+/##',
    'objekt1/wohnung2/#/feuchte',
    '#',
];

export const ValidTopicPublish: string[] = [
    'objekt1',
    'objekt1/wohnung1/schlafzimmer/luftdruck',
    'objekt1/wohnung1',
    'objekt1/wohnung1/temperatur',
    'objekt1/wohnung1/wohnzimmer/luftdruck',
];

export const InvalidTopicPublish: string[] = [
    '/dsa',
    '/',
    '+/++/wohnzimmer/#',
    '+/##',
    '+/wohnung1/+/#',
    '+/+/uwe/+/wohnzimmer/#',
    '+',
    '+/#',
    'affe/+/#',
    'objekt1/wohnung2/schlafzimmer/#',
    '+/+/+/+/#'
];

export const ValidTopic: string[] = [
    '+/wohnung1/+/#',
    '+/+/uwe/+/wohnzimmer/#',
    '+/+/+/+/#',
    'objekt1/wohnung1',
    'objekt1/wohn/ung1',
    'objekt1/wohnung1/temperatur',
    'objekt1/wohnung1/wohnzimmer/luftdruck',
    'objekt1/',
];

export const InvalidTopic: string[] = [
    '+/wohnung1/++/#',
    '+/-+/uwe+/+/wohnzimmer/#',
    '+//+/#',
    'objekt1/wohnung1!',
    'objekt1/#/temperatur',
    '#',
];

type TopicAndRegex = { t: string, r: RegExp };
export const TopicAndRegexes: TopicAndRegex[] = [
    {t: 'home/+/wohnung1/+/#', r: /^home\/[\d\w]+\/wohnung1\/[\d\w]+[\d\w\/]*$/},
    {t: '+/+/uwe/+/wohnzimmer/#', r: /^[\d\w]+\/[\d\w]+\/uwe\/[\d\w]+\/wohnzimmer[\d\w\/]*$/},
    {t: 'objekt1/wohnung1/wohnzimmer/feuchte', r: /^objekt1\/wohnung1\/wohnzimmer\/feuchte$/},
    {t: '+', r: /^[\d\w]+$/},
    {t: 'affe/+/#', r: /^affe\/[\d\w]+[\d\w\/]*$/},
    {t: '+/#', r: /^[\d\w]+[\d\w\/]*$/},
    {t: 'home/#', r: /^home[\d\w\/]*$/},
];

describe('Topic', () => {

    it('Topic.match()', () => {
        // examples from: https://docs.solace.com/Open-APIs-Protocols/MQTT/MQTT-Topics.htm
        expect(Topic.match('home/kitchen/+', 'home/kitchen/temperature')).toBe(true);
        expect(Topic.match('home/kitchen/+', 'home/kitchen/humidity')).toBe(true);
        expect(Topic.match('home/kitchen/+', 'home/bedroom/temperature')).toBe(false);
        expect(Topic.match('home/kitchen/+', 'business/kitchen/humidity')).toBe(false);
        expect(Topic.match('home/+/temperature', 'home/kitchen/temperature')).toBe(true);
        expect(Topic.match('home/+/temperature', 'home/bedroom/temperature')).toBe(true);
        expect(Topic.match('home/+/temperature', 'home/kitchen/humidity')).toBe(false);
        expect(Topic.match('home/+/temperature', 'business/kitchen/humidity')).toBe(false);
        expect(Topic.match('+', 'home')).toBe(true);
        expect(Topic.match('+', 'busines')).toBe(true);
        expect(Topic.match('+', 'home/kitchen')).toBe(false);
        expect(Topic.match('+', 'home/kitchen/temperature')).toBe(false);
        expect(Topic.match('+', 'business/lobby')).toBe(false);
        expect(Topic.match('home/#', 'home')).toBe(true);
        expect(Topic.match('home/#', 'home/')).toBe(true);
        expect(Topic.match('home/#', 'home/kitchen/temperature')).toBe(true);
        expect(Topic.match('home/#', 'home/bedroom/humidity')).toBe(true);
        expect(Topic.match('home/#', 'business/lobby')).toBe(false);
    });

    it('Topic.topicToRegex() valid', () => {
        TopicAndRegexes.forEach((topicAndRegex: TopicAndRegex) => {
            const topic: string = topicAndRegex.t;
            const regex: RegExp = topicAndRegex.r;
            expect(Topic.toRegex.bind(Topic, topic)).not.toThrow();
            const resultRegex: RegExp = Topic.toRegex(topic);
            expect(resultRegex).toBeInstanceOf(RegExp);
            expect(resultRegex.source).toEqual(regex.source);
        });
    });

    it('Topic.topicToRegex() invalid', () => {
        InvalidTopicSubscribe.forEach((topic: string) => {
            expect(Topic.toRegex.bind(Topic, topic)).toThrow(Topic.ERROR_INVALID_TOPIC);
        });
    });

    it('Topic.REGEX_TOPIC_SUBSCRIBE valid', () => {
        ValidTopicSubscribe.forEach((topic: string) => {
            expect(topic).toMatch(Topic.REGEX_TOPIC_SUBSCRIBE);
        });
    });

    it('Topic.REGEX_TOPIC_SUBSCRIBE invalid', () => {
        InvalidTopicSubscribe.forEach((topic: string) => {
            expect(topic).not.toMatch(Topic.REGEX_TOPIC_SUBSCRIBE);
        });
    });

    it('Topic.REGEX_TOPIC_PUBLISH valid', () => {
        ValidTopicPublish.forEach((topic: string) => {
            expect(topic).toMatch(Topic.REGEX_TOPIC_PUBLISH);
        });
    });

    it('Topic.REGEX_TOPIC_PUBLISH invalid', () => {
        InvalidTopicPublish.forEach((topic: string) => {
            expect(topic).not.toMatch(Topic.REGEX_TOPIC_PUBLISH);
        });
    });

    it('Topic.validateSubscribe() valid', () => {
        ValidTopicSubscribe.forEach((topic: string) => {
            expect(Topic.validateSubscribe(topic)).toBe(true);
        });
    });

    it('Topic.validateSubscribe() invalid', () => {
        InvalidTopicSubscribe.forEach((topic: string) => {
            expect(Topic.validateSubscribe(topic)).toBe(false);
        });
    });

    it('Topic.validatePublish() valid', () => {
        ValidTopicPublish.forEach((topic: string) => {
            expect(Topic.validatePublish(topic)).toBe(true);
        });
    });

    it('Topic.validatePublish() invalid', () => {
        InvalidTopicPublish.forEach((topic: string) => {
            expect(Topic.validatePublish(topic)).toBe(false);
        });
    });

    it('Topic.validate() valid', () => {
        ValidTopic.forEach((topic: string) => {
            expect(Topic.validate(topic)).toBe(true);
        });
    });

    it('Topic.validate() invalid', () => {
        InvalidTopic.forEach((topic: string) => {
            expect(Topic.validate(topic)).toBe(false);
        });
    });

});

