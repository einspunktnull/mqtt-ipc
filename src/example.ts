import 'source-map-support/register';
import {Broker} from './lib/Broker';
import {Client} from './lib/Client';
import child_process from 'child_process';

export const BROKER_ID: string = 'broker';
export const CLIENT_ID: string = 'client';

type ClientArgs = {
    suffix: string;
    type: 'sub' | 'pub';
    val: string;
};

(async () => {

    const args: string[] = process.argv.slice(2);

    if (args.length > 0) {
        const suffix: string = args[0];
        const type: 'sub' | 'pub' = <'sub' | 'pub'>args[1];
        const val: string = args[2];
        return runClient(suffix, type, val);
    }
    await runBroker();

    const clientArgses: ClientArgs[] = [
        {suffix: 'uwe', type: "sub", val: '+/text'},
        {suffix: 'sven', type: "pub", val: 'sven/text'},
        {suffix: 'ralf', type: "pub", val: 'ralf/text'},
    ];
    clientArgses.forEach((clientArgs: ClientArgs) => {
        child_process.fork('./dist/example.js', [
            clientArgs.suffix,
            clientArgs.type,
            clientArgs.val
        ]);
    });


})();

async function runBroker(): Promise<void> {
    // console.log('runBroker');
    const broker: Broker = new Broker(BROKER_ID);
    await broker.initialize();
}

async function runClient(suffix: string, type: 'sub' | 'pub', val: string): Promise<void> {
    // console.log('runClient', suffix, type, val);
    const client: Client = new Client(BROKER_ID, CLIENT_ID + '_' + suffix);
    await client.initialize();
    if (type === "sub") {
        const subId:string = await client.subscribe(val, (data: any) => {
            console.log(suffix, 'onSubbbi', val, data);
        });
        console.log(suffix,'subscribed',subId);
    } else {
        setTimeout(async () => {
            // console.log(suffix, 'pub');
            const published:string = await client.publish(val, 'hulehule sagt ' + suffix);
            console.log(suffix,'published',published);
        }, 1000);
    }
}
